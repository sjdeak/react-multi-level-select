import React from 'react';
import { Fragment } from 'react';
import { Select } from 'antd';
import LOCATIONS from './const/locations';

const Option = Select.Option;

export default class MultiLevelSelect extends React.Component {
  state = {
    province: null,
    city: null,
    district: null
  };

  handleProvinceChange = (value) => {
    this.setState({
      province: value,
      city: null,
      district: null
    });
  };

  handleCityChange = (value) => {
    this.setState({
      city: value,
      district: null
    });
  };

  handleDistrictChange = (value) => {
    this.setState({district: value}, () => {
      alert('三级选择完毕');
    });
  };

  renderCityOptions = () => {
    const options = LOCATIONS[this.state.province].options;

    return Object.keys(options).map(value =>
      <Option key={value}>{options[value].label}</Option>)
  };

  renderDistrictOptions = () => {
    const cityOptions = LOCATIONS[this.state.province].options;
    const districtOptions = cityOptions[this.state.city].options;

    console.log('districtOptions:', districtOptions);

    return Object.keys(districtOptions).map(value =>
      <Option key={value}>{districtOptions[value]}</Option>)
  };

  render() {
    return <Fragment>
      <div>
        <Select style={{width: '200px' }}
                onChange={this.handleProvinceChange}
                value={this.state.province}
        >
          {
            Object.keys(LOCATIONS).map(name =>
              <Option key={name}>{LOCATIONS[name].label}</Option>)
          }
        </Select>省
      </div>

      <div>
        <Select style={{width: '200px' }}
                onChange={this.handleCityChange}
                disabled={this.state.province === null}
                value={this.state.city}
        >
          {
            this.state.province && this.renderCityOptions()
          }
        </Select>市
      </div>

      <div>
        <Select style={{width: '200px' }}
                onChange={this.handleDistrictChange}
                disabled={this.state.city === null}
                value={this.state.district}
        >
          {
            this.state.city && this.renderDistrictOptions()
          }
        </Select>区
      </div>

      <div>
        当前state: {this.state.province}省  {this.state.city}市  {this.state.district}区
      </div>
    </Fragment>;
  }
}