const LOCATIONS = {
  'ZheJiang': {
    label: '浙江',
    options: {
      'Hangzhou': {
        label: '杭州',
        options: {
          'Xiacheng': '下城',
          'Jianggan': '江干',
          'Xihu': '西湖'
        }
      },
      'Wenzhou': {
        label: '温州',
        options: {
          'Lucheng': '鹿城',
          "Longwan": '龙湾',
          "Dongtou": '洞头'
        }
      },
      'Ningbo': {
        label: '宁波',
        options: {
          'Jiangbei': '江北',
          "Jiangdong": '江东',
          "Haishu": '海曙'
        }
      }
    }
  },
  'JiangSu': {
    label: '江苏',
    options: {
      'Nanjing': {
        label: '南京',
        options: {
          'QinHuai': '秦淮',
          'Jianye': '建邺',
          'Gulou': '鼓楼'
        }
      },
      'Yangzhou': {
        label: '扬州',
        options: {
          'Guangling': '广陵',
          'Jiangdu': '江都',
        }
      }
    }
  }
};

export default LOCATIONS;