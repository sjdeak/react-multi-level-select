import React, { Component } from 'react';
import MultiLevelSelect from "./MultiLevelSelect";

class App extends Component {
  render() {
    return (
      <div className="App">
        <MultiLevelSelect />
      </div>
    );
  }
}

export default App;
